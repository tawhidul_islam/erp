package com.asl.entity;

import java.io.Serializable;

import lombok.Data;

@Data
public class LandMemberInfoPK implements Serializable{

	private static final long serialVersionUID = -5569014195337770892L;
	private String xmember;
	private String zid;

}
